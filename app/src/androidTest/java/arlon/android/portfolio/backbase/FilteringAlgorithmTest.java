package arlon.android.portfolio.backbase;

import android.util.Log;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import arlon.android.portfolio.backbase.adapters.CityListAdapter;
import arlon.android.portfolio.backbase.objects.City;

/**
 * Created by Arlon Mukeba on 11/21/2017.
 */

public class FilteringAlgorithmTest {

    @Test
    public void testCityFilteringByName() {
        String[] cityNames = { "Hurzuf", "Novinki", "Gorkhā", "State of Haryāna", "Holubynka",
                "Bāgmatī Zone", "Mar’ina Roshcha", "Republic of India", "Kathmandu", "Laspi",
                "Merida", "Vinogradovo", "Qarah Gawl al ‘Ulyā", "Cherkizovo", "Alupka",
                "Lichtenrade", "Zavety Il’icha", "‘Azriqam", "Ghūra", "Tyuzler", "Zaponor’ye",
                "Il’ichëvka", "Partyzans’ke", "Yurevichi", "Gumist’a", "Ptitsefabrika", "Orekhovo",
                "Birim", "Priiskovyy", "Dzhaga", "Tret’ya Rota", "Ruislip", "Karow", "Gatow",
                "Mkuze", "Lhasa", "İstanbul", "Mao", "Russian Federation", "De-Friz", "Rumbak",
                "Vavibet", "Surtagān Chib", "Rīgas Rajons", "Verkhneye Shchekotikhino", "Bucha",
                "Republic of Poland", "Kuchary", "Brumaire", "Ishikawa-ken", "Matoba",
                "Pya", "Kalanac", "Federal Republic of Germany", "Land Nordrhein-Westfalen",
                "Mutaykutan", "Nalchik", "Kolganov", "Rybatskiy", "Bellara", "Bartlett",
                "Rietfontein", "Hardap", "Botswana", "El Destierro", "Jones Crossroads",
                "Vernon Parish", "Pennick", "Black Bear Spring", "Bee House", "Morden", "Nasirotu",
                "Sisali", "Puntan", "Tsiémé-Mandiélé","Masama", "Purukcahu", "Néméyong II",
                "Pondok Genteng", "Mbongoté", "Amiling", "Kélkoto", "Angetu", "Massa", "Tumko",
                "Moskva", "Japan", "Hokkaidō", "Sanggrahan" };

        ArrayList<City> cities = buildList(cityNames);

        ArrayList<City> filteredCities = buildList(new String[] { "Ruislip", "Russian Federation",
                "Rumbak" });

        assertEquals("Algorithm Test Failed!", filteredCities, getFilteredList(cities, "RU"));

        filteredCities = buildList(new String[] { "Matoba", "Purukcahu", "Angetu" });

        assertEquals("Algorithm Test Failed!", filteredCities, getFilteredList(cities, "Ma"));
    }

    private ArrayList<City> buildList(final String[] cityNames) {
        return new ArrayList<City>() {
            {
                for (String cityName : cityNames) {
                    add(new City(cityName));
                }
            }
        };
    }

    private ArrayList<City> getFilteredList(ArrayList<City> originalList, String constraint) {
        ArrayList<City> cities = originalList;
        String filter = constraint.toString();

        if (constraint.length() != 0) {
            cities = new ArrayList<City>();

            for (City city : originalList) {
                if (city.getName().toLowerCase().startsWith(filter.toLowerCase())) {
                    cities.add(city);
                }
            }
        }

        return cities;
    }
}