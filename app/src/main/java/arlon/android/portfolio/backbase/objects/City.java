package arlon.android.portfolio.backbase.objects;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import arlon.android.portfolio.backbase.helpers.JsonHelper;

/**
 * Created by Arlon Mukeba on 11/20/2017.
 */

public class City implements Parcelable, Comparable<City> {

    private static final String ID = "_id";
    private static final String COUNTRY = "country";
    private static final String NAME = "name";
    private static final String COORD = "coord";
    private static final String LONG = "lon";
    private static final String LAT = "lat";

    private String mId = "";
    private String mCountry = "";
    private String mName = "";
    private double mLongitude = 0.0;
    private double mLatitude = 0.0;

    private static ArrayList<City> mCities = new ArrayList<>();

    public City() {
    }

    public City(String name) {
        setName(name);
    }

    public City(Parcel in) {
        setId(in.readString());
        setCountry(in.readString());
        setName(in.readString());
        setLongitude(in.readDouble());
        setLatitude(in.readDouble());
    }

    public void setId(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public static final Creator<City> CREATOR = new Creator<City>() {

        @Override
        public City createFromParcel(Parcel source) {
            return new City(source);
        }

        @Override
        public City[] newArray(int size) {
            return new City[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getId());
        dest.writeString(getCountry());
        dest.writeString(getName());
        dest.writeDouble(getLongitude());
        dest.writeDouble(getLatitude());
    }

    public String toString () {
        return new StringBuffer("CITY: ").append("\n\n")
                .append("ID: ").append(mId).append("\n")
                .append("Country: ").append(mCountry).append("\n")
                .append("Name: ").append(mName).append("\n")
                .append("Longitude: ").append(mLongitude).append("\n")
                .append("Latitude: ").append(mLatitude).append("\n").toString();
    }

    public static ArrayList<City> getCityList(Context context) {
        if (mCities.isEmpty()) {
            mCities = loadCityList(context);
        }

        return mCities;
    }

    private static ArrayList<City> loadCityList(Context context) {
        ArrayList<City> cities = new ArrayList<City>();

        try {
            String jsonString = readCityFile(context);
            cities = buildCityList(jsonString);
        } catch (IOException ioexception) {
        } catch (JSONException jsonexception) {
        }

        return cities;
    }

    private static String readCityFile(Context context) throws IOException {
        StringBuilder jsonString = new StringBuilder();
        InputStream inputStream = context.getAssets().open("cities.json");

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,
                "UTF-8"));

        String fileLine;

        while ((fileLine = bufferedReader.readLine()) != null) {
            jsonString.append(fileLine);
        }

        bufferedReader.close();

        return jsonString.toString();
    }

    private static ArrayList<City> buildCityList(String jsonString) throws JSONException {
        ArrayList<City> cities = new ArrayList<City>();
        JSONArray jsonArray = new JSONArray(jsonString);

        for (int i = 0; i < jsonArray.length(); i++) {
            cities.add(buildCity(jsonArray.getJSONObject(i)));
        }

        return cities;
    }

    private static City buildCity(final JSONObject cityJson) {
        City city = new City() {
            {
                setId(JsonHelper.getString(cityJson, ID));
                setCountry(JsonHelper.getString(cityJson, COUNTRY));
                setName(JsonHelper.getString(cityJson, NAME));

                JSONObject coord = JsonHelper.getObject(cityJson, COORD);

                setLongitude(JsonHelper.getDouble(coord, LONG));
                setLatitude(JsonHelper.getDouble(coord, LAT));
            }
        };

        return city;
    }

    public boolean equals(Object object) {
        if (!(object instanceof City)) return false;

        return (((City) object).getName().equalsIgnoreCase(getName()));
    }

    @Override
    public int compareTo(City city) {
        return getName().compareTo(city.getName());
    }
}