package arlon.android.portfolio.backbase.interfaces;

import java.util.Hashtable;

/**
 * Created by Arlon Mukeba on 11/20/2017.
 */

public interface FragmentSwitcher {

    void switchFragment(String tag, Hashtable<String, Object> parameters);
}
