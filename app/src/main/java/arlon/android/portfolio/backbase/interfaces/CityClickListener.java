package arlon.android.portfolio.backbase.interfaces;

import arlon.android.portfolio.backbase.objects.City;

/**
 * Created by Arlon Mukeba on 11/20/2017.
 */

public interface CityClickListener {

    public void CityClicked(City city);
}