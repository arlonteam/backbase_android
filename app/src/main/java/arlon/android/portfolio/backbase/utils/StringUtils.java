package arlon.android.portfolio.backbase.utils;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;

/**
 * Created by Arlon Mukeba on 11/20/2017.
 */

public class StringUtils {

    public static SpannableStringBuilder setColourTag(String inputString, String link, int color) {
        return setColourTag(new SpannableStringBuilder(inputString), link, color);
    }

    public static SpannableStringBuilder setColourTag(SpannableStringBuilder ssb, String link,
                                                      int color) {

        int indexOfPattern = ssb.toString().indexOf(link);

        ssb.setSpan(new ForegroundColorSpan(color), indexOfPattern,
                (indexOfPattern + (link.length())), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return ssb;
    }

    public static SpannableStringBuilder setStyleTag(String inputString, String link, int style) {
        return setStyleTag(new SpannableStringBuilder(inputString), link, style);
    }

    public static SpannableStringBuilder setStyleTag(SpannableStringBuilder ssb, String link,
                                                      int style) {

        int indexOfPattern = ssb.toString().indexOf(link);

        ssb.setSpan(new StyleSpan(style), indexOfPattern, (indexOfPattern + (link.length())),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return ssb;
    }
}