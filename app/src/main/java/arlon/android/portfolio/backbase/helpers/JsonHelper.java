package arlon.android.portfolio.backbase.helpers;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Arlon Mukeba on 11/20/2017.
 */

public class JsonHelper {

    public static int getInt(JSONObject jsonObject, String key) {
        int value = 0;

        if (jsonObject.has(key)) {
            try {
                value = jsonObject.getInt(key);
            } catch (Exception e) {

            }
        }

        return value;
    }

    public static double getDouble(JSONObject jsonObject, String key) {
        double value = 0.0;

        if (jsonObject.has(key)) {
            try {
                value = jsonObject.getDouble(key);
            } catch (Exception e) {

            }
        }

        return value;
    }

    public static String getString(JSONObject jsonObject, String key) {
        String value = "";

        if (jsonObject.has(key)) {
            try {
                value = jsonObject.getString(key);
            } catch (Exception e) {
            }
        }

        return value;
    }

    public static JSONObject getObject(JSONObject jsonObject, String key) {
        JSONObject object = new JSONObject();

        if (jsonObject.has(key)) {
            try {
                object = jsonObject.getJSONObject(key);
            } catch (Exception e) {

            }
        }

        return object;
    }

    public static JSONArray getArray(JSONObject jsonObject, String key) {
        JSONArray array = new JSONArray();

        if (jsonObject.has(key)) {
            try {
                array = jsonObject.getJSONArray(key);
            } catch (Exception e) {
            }
        }

        return array;
    }
}