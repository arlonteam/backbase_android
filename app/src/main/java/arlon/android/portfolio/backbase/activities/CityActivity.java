package arlon.android.portfolio.backbase.activities;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Hashtable;
import arlon.android.portfolio.backbase.R;
import arlon.android.portfolio.backbase.fragments.BaseFragment;
import arlon.android.portfolio.backbase.fragments.CityDetailsFragment;
import arlon.android.portfolio.backbase.fragments.CityListFragment;
import arlon.android.portfolio.backbase.interfaces.FragmentSwitcher;
import arlon.android.portfolio.backbase.objects.City;

public class CityActivity extends AppCompatActivity implements FragmentSwitcher {

    private String mFragmentTag = CityListFragment.TAG;
    private BaseFragment mCurrentFragment = null;
    private TextView mTvHeading = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);
        setupToolbar();
        switchFragment(CityListFragment.TAG, new Hashtable<String, Object>());
    }

    private void setupToolbar() {
        RelativeLayout customHeadingLayout = (RelativeLayout) LayoutInflater.from(this).inflate(
            R.layout.custom_toolbar_title, null);

        mTvHeading = (TextView) customHeadingLayout.findViewById(R.id.tvToolbarTitle);

        getSupportActionBar().setCustomView(customHeadingLayout);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void setHeadings(String title, boolean visible) {
        mTvHeading.setText(title);
        toggleHomeIcon(visible);
    }

    private void toggleHomeIcon(boolean visible) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void switchFragment(String fragmentTag, Hashtable<String, Object> parameters) {
        BaseFragment fragment = null;
        String title = getString(R.string.city_list_title);
        boolean showHome = false;

        switch (fragmentTag) {
            case CityListFragment.TAG:
                fragment = CityListFragment.newInstance();
                break;

            case CityDetailsFragment.TAG:
                fragment = CityDetailsFragment.newInstance(parameters);
                City city = (City) parameters.get(CityDetailsFragment.CITY_PARAM);

                title = new StringBuffer(city.getName()).append(", ").append(city.getCountry())
                        .toString();

                showHome = true;
                break;
        }

        if (fragment == null) {
            return;
        }

        fragment.setFragmentSwitcher(this);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        transaction.replace(R.id.content_frame, fragment);

        mCurrentFragment = fragment;

        transaction.commit();

        setHeadings(title, showHome);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mCurrentFragment instanceof CityListFragment) {
            handleListEvents();
        } else {
            switchFragment(CityListFragment.TAG, new Hashtable<String, Object>());
        }
    }

    private void handleListEvents() {
        CityListFragment currentFragment = (CityListFragment) mCurrentFragment;

        if (currentFragment.getScreenMode() != CityListFragment.SCREEN_MODE.DEFAULT) {
            currentFragment.resetMode();
        } else {
            finish();
        }
    }
}