package arlon.android.portfolio.backbase.fragments;

import android.content.Context;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Hashtable;

import arlon.android.portfolio.backbase.R;
import arlon.android.portfolio.backbase.interfaces.FragmentSwitcher;
import arlon.android.portfolio.backbase.objects.City;

public class BaseFragment extends Fragment implements LoaderManager.LoaderCallbacks {

    public static final String TAG = "BASE_FRAGMENT";
    public static final String FRAGMENT_PARAMS = "FRAGMENT_PARAMS";

    protected Hashtable<String, Object> mParams = new Hashtable<String, Object>();
    protected Context mContext = null;
    protected FragmentSwitcher mFragmentSwitcher = null;

    public BaseFragment() {
    }

    public static BaseFragment newInstance(Hashtable<String, Object> params) {
        BaseFragment baseFragment = new BaseFragment();
        Bundle args = new Bundle();
        args.putSerializable(FRAGMENT_PARAMS, params);
        baseFragment.setArguments(args);
        return baseFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getContext();

        if (getArguments() != null) {
            mParams = (Hashtable<String, Object>) getArguments().getSerializable(FRAGMENT_PARAMS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_city_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initFragment(savedInstanceState);
    }

    protected void initFragment(@Nullable Bundle savedInstanceState) {
        getLoaderManager().initLoader(TAG.hashCode(), null, this).forceLoad();
    }

    public void setFragmentSwitcher(FragmentSwitcher fragmentSwitcher) {
        mFragmentSwitcher = fragmentSwitcher;
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return getLoader();
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
    }

    protected DataFetcher getLoader() {
        return new DataFetcher(getContext());
    }

    protected static class DataFetcher extends AsyncTaskLoader<Object> {

        protected Context mContext = null;

        public DataFetcher(Context context) {
            super(context);

            mContext = context;
        }

        @Override
        public Object loadInBackground() {
            return null;
        }

        @Override
        public void deliverResult(Object data) {
            super.deliverResult(data);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}