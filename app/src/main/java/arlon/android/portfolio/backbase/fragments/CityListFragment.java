package arlon.android.portfolio.backbase.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Hashtable;

import arlon.android.portfolio.backbase.R;
import arlon.android.portfolio.backbase.adapters.CityListAdapter;
import arlon.android.portfolio.backbase.interfaces.CityClickListener;
import arlon.android.portfolio.backbase.objects.City;

public class CityListFragment extends BaseFragment {

    public static final String TAG = "CITY_LIST_FRAGMENT";

    public static enum SCREEN_MODE { DEFAULT, SEARCH };

    private CityListAdapter mCityListAdapter = null;
    private RecyclerView mRvCities = null;
    private LinearLayout mLlCities = null;
    private RelativeLayout mRlSearch = null;
    private EditText mEdtSearch = null;
    private ImageView mIvCancelSearch = null;
    private ProgressBar mPbLoading = null;
    private SCREEN_MODE mScreenMode = SCREEN_MODE.DEFAULT;

    public CityListFragment() {
    }

    public static CityListFragment newInstance() {
        return new CityListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_city_list, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menu.clear();

        menuInflater.inflate(R.menu.menu_city_list_fragment, menu);

        super.onCreateOptionsMenu(menu, menuInflater);
    }

    public void onPrepareOptionsMenu(Menu menu) {
        boolean state = mScreenMode.equals(SCREEN_MODE.DEFAULT);

        menu.findItem(R.id.action_search).setVisible(state);
        menu.findItem(R.id.action_reset).setVisible(!state);

        mRlSearch.setVisibility((!state) ? View.VISIBLE : View.GONE);

        super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            switchToSearchMode();
            return true;
        }  else if (item.getItemId() == R.id.action_reset) {
            resetMode();
            return true;
        }

        return false;
    }

    public void resetMode() {
        mScreenMode = SCREEN_MODE.DEFAULT;
        getActivity().invalidateOptionsMenu();
        mEdtSearch.setText("");
        filterList("");
    }

    private void switchToSearchMode() {
        mScreenMode = SCREEN_MODE.SEARCH;
        getActivity().invalidateOptionsMenu();
    }

    private void filterList(String constraint) {
        mCityListAdapter.getFilter().filter(constraint);
        mCityListAdapter.notifyDataSetChanged();

        mRvCities.scrollToPosition(0);
    }

    protected void initFragment(@Nullable Bundle savedInstanceState) {
        setupViews();
        mPbLoading.setVisibility(View.VISIBLE);
        super.initFragment(savedInstanceState);
    }

    private void setupViews() {
        mPbLoading = (ProgressBar) getView().findViewById(R.id.pbLoading);

        mRlSearch = (RelativeLayout) getView().findViewById(R.id.rlSearch);

        mEdtSearch = (EditText) getView().findViewById(R.id.edtSearch);
        mEdtSearch.addTextChangedListener(new EditTextWatcher());

        mIvCancelSearch = (ImageView) getView().findViewById(R.id.ivSearchCancel);
        mIvCancelSearch.setOnClickListener(new SearchCancelClicked());

        mCityListAdapter = new CityListAdapter(getContext());
        mCityListAdapter.setCitySelectedListener(new CityClicked());
        mRvCities = (RecyclerView) getView().findViewById(R.id.rvCities);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());

        mRvCities.setLayoutManager(mLayoutManager);
        mRvCities.setItemAnimator(new DefaultItemAnimator());
        mRvCities.setAdapter(mCityListAdapter);

        mLlCities = (LinearLayout) getView().findViewById(R.id.llCities);
    }

    public SCREEN_MODE getScreenMode() {
        return mScreenMode;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
//        Log.d("LISTCITIES", ((ArrayList<City>) data).toString());
        mCityListAdapter.setCities((ArrayList<City>) data);
        mCityListAdapter.notifyDataSetChanged();
        mPbLoading.setVisibility(View.GONE);
        mLlCities.setVisibility((((ArrayList<City>) data).isEmpty()) ? View.GONE : View.VISIBLE);
    }

    protected DataFetcher getLoader() {
        return new CityListFetcher(getContext());
    }

    private static class CityListFetcher extends DataFetcher {

        public CityListFetcher(Context context) {
            super(context);
        }

        @Override
        public Object loadInBackground() {
            return City.getCityList(mContext);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private class EditTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            filterList(s.toString());
        }
    }

    private class SearchCancelClicked implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            resetMode();
        }
    }

    private class CityClicked implements CityClickListener {

        @Override
        public void CityClicked(City city) {
            Hashtable<String, Object> params = new Hashtable<String, Object>();
            params.put(CityDetailsFragment.CITY_PARAM, city);
            mFragmentSwitcher.switchFragment(CityDetailsFragment.TAG, params);
        }
    }
}