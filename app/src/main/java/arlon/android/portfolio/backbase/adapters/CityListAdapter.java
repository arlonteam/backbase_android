package arlon.android.portfolio.backbase.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import arlon.android.portfolio.backbase.R;
import arlon.android.portfolio.backbase.interfaces.CityClickListener;
import arlon.android.portfolio.backbase.objects.City;
import arlon.android.portfolio.backbase.utils.StringUtils;

/**
 * Created by Arlon Mukeba on 11/20/2017.
 */

public class CityListAdapter extends RecyclerView.Adapter<CityListAdapter.CityViewHolder>
        implements Filterable {

    protected Context mContext = null;
    protected CityClickListener mListener = null;

    private ArrayList<City> mCities = new ArrayList<>();
    private ArrayList<City> mClonedCities = new ArrayList<>();

    public CityListAdapter(Context context) {
        mContext = context;
    }

    public void setCities(ArrayList<City> cities) {
        mCities = /*City.getCityList(mContext);*/ cities;
        Collections.sort(mCities);
        mClonedCities = mCities;
    }

    public ArrayList<City> getCities() {
        return mCities;
    }

    public void setCitySelectedListener(CityClickListener listener) {
        mListener = listener;
    }

    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout itemLayout = (RelativeLayout) LayoutInflater.from(mContext).inflate(
            R.layout.layout_city, parent, false);

        return new CityViewHolder(itemLayout);
    }

    public void onBindViewHolder(final CityViewHolder cityViewHolder, int position) {
        City city = mCities.get(position);
        cityViewHolder.mTvId.setText(city.getId());

        String name = new StringBuilder(city.getName()).append(", ").append(city.getCountry())
                .toString();

        cityViewHolder.mTvName.setText(StringUtils.setStyleTag(name, new StringBuilder(
                city.getName()).append(", ").toString(), Typeface.BOLD));

        int color = mContext.getResources().getColor(R.color.red);

        String longitude = new StringBuilder("Lon: ").append(city.getLongitude()).toString();
        cityViewHolder.mTvLong.setText(StringUtils.setColourTag(longitude, "Lon: ", color));

        String latitude = new StringBuilder("Lat: ").append(city.getLatitude()).toString();
        cityViewHolder.mTvLat.setText(StringUtils.setColourTag(latitude, "Lat: ", color));

        cityViewHolder.mTvView.setOnClickListener(new CityClicked(city));
    }

    public int getItemCount() {
        return ((mCities == null) || (mCities.isEmpty())) ? 0 : mCities.size();
    }

    private class CityClicked implements View.OnClickListener {

        private City mCity = null;

        public CityClicked(City city) {
            mCity = city;
        }

        @Override
        public void onClick(View v) {
            mListener.CityClicked(mCity);
        }
    }

    @Override
    public Filter getFilter() {
        return new CityFilter();
    }

    public class CityViewHolder extends RecyclerView.ViewHolder {

        public View mView;
        public TextView mTvId;
        public TextView mTvName;
        public TextView mTvLong;
        public TextView mTvLat;
        public TextView mTvView;

        public CityViewHolder(View view) {
            super(view);

            mView = view;
            mTvId = (TextView) mView.findViewById(R.id.tvId);
            mTvName = (TextView) mView.findViewById(R.id.tvName);
            mTvLong = (TextView) mView.findViewById(R.id.tvLong);
            mTvLat = (TextView) mView.findViewById(R.id.tvLat);
            mTvView = (TextView) mView.findViewById(R.id.tvView);
        }
    }

    private class CityFilter extends Filter {

        private FilterResults mResults = new FilterResults();

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<City> cities = mClonedCities;
            String filter = constraint.toString();

            if (constraint.length() != 0) {
                cities = new ArrayList<City>();

                for (City city : mClonedCities) {
                    if (city.getName().toLowerCase().startsWith(filter.toLowerCase())) {
                        cities.add(city);
                    }
                }
            }

            mResults.values = cities;
            mResults.count = cities.size();

            return mResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mCities = (ArrayList<City>)results.values;

            notifyDataSetChanged();
        }
    }
}