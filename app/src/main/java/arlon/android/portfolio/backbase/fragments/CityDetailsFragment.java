package arlon.android.portfolio.backbase.fragments;

import android.content.Context;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.Hashtable;
import arlon.android.portfolio.backbase.R;
import arlon.android.portfolio.backbase.objects.City;

public class CityDetailsFragment extends BaseFragment {

    public static final String TAG = "CITY_DETAILS_FRAGMENT";
    public static final String CITY_PARAM = "CITY_PARAM";

    private City mCity = null;
    private MapView mMapView = null;

    public CityDetailsFragment() {
    }

    public static CityDetailsFragment newInstance(Hashtable<String, Object> params) {
        CityDetailsFragment cityDetailsFragment = new CityDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(FRAGMENT_PARAMS, params);
        cityDetailsFragment.setArguments(args);
        return cityDetailsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mParams != null) {
            if (mParams.containsKey(CITY_PARAM)) {
                mCity = (City) mParams.get(CITY_PARAM);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_city_details, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menu.clear();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        setupMap(savedInstanceState);
        super.onViewCreated(view, savedInstanceState);
    }

    private void setupMap(@Nullable Bundle savedInstanceState) {
        mMapView = (MapView) getView().findViewById(R.id.mapview);

        if (googleServicesAvailable()) {
            mMapView.onCreate(savedInstanceState);
        }
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {
        if (googleServicesAvailable()) {
            if (mMapView != null) {
                mMapView.getMapAsync(new MapReadyListener());
            }
        }
    }

    protected DataFetcher getLoader() {
        return new CityDetailsFetcher(getContext());
    }

    private static class CityDetailsFetcher extends DataFetcher {

        public CityDetailsFetcher(Context context) {
            super(context);
        }

        @Override
        public Object loadInBackground() {
            return null;
        }
    }

    private class MapReadyListener implements OnMapReadyCallback {

        @Override
        public void onMapReady(GoogleMap googleMap) {
            Geocoder geocoder = new Geocoder(mContext);

            if (geocoder == null) return;

            LatLng location = new LatLng(mCity.getLatitude(), mCity.getLongitude());

            googleMap.addMarker(new MarkerOptions()
                .title(new StringBuffer(mCity.getName()).append(", ").append(mCity.getCountry())
                    .toString()).snippet(new StringBuffer("Longitude: ").append(mCity.getLongitude())
                        .append("\nLatitude").append(mCity.getLatitude()).toString())
                            .icon(BitmapDescriptorFactory.fromResource(
                                R.drawable.ic_location)).anchor(0.0f, 1.0f).position(
                                    location));

            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            googleMap.getUiSettings().setZoomGesturesEnabled(false);
            googleMap.getUiSettings().setZoomControlsEnabled(false);
            MapsInitializer.initialize(mContext);
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(location);
            LatLngBounds bounds = builder.build();
            int padding = 0;

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            googleMap.moveCamera(cameraUpdate);
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(5.0f));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    public boolean googleServicesAvailable() {
        int isAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getContext());

        return (isAvailable == ConnectionResult.SUCCESS);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}